@extends('layouts.app')

@section('title', '| Edit Role')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>
    <h1><i class='fa fa-key'></i> Modifier role: {{$role->name}}</h1>
    <hr>

    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

    <div class="form-group  {{ $errors->has('name') ? 'has-error' :'' }} ">
        {{ Form::label('name', 'Nom de role') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
          @if ($errors->has ('name'))
                          <span class="help-block">{{ $errors->first('name')}}</span>
                          @endif
    </div>

    <h5><b>Choisir permission </b></h5>
        <div class="form-group  {{ $errors->has('permissions') ? 'has-error' :'' }} ">
    @foreach ($permissions as $permission)

        {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
        {{Form::label($permission->name, ucfirst($permission->name)) }}<br>
    @endforeach
      @if ($errors->has ('permissions'))
                          <span class="help-block">{{ $errors->first('permissions')}}</span>
                          @endif
    <br>
    {{ Form::submit('Modifier', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}    
</div>

@endsection