@extends('layouts.app')

@section('title', '| Create New voiture')

@section('content')
    <div class="row" style="margin-left: 15%;">
        <div class="col-md-8 col-md-offset-2">

        <h1><i class='fa fa-car'>Ajouter une voiture </i></h1>
        <hr>

  
        {{ Form::open(array('route' => 'voitures.store')) }}

                    <div class="form-group {{ $errors->has('marque') ? 'has-error' :'' }}">

                        <label for="marque"><h4>Marque de voiture</h4> </label>
                        <select name="marque">
                            <option value="">Selectionner</option>
                            <option value="Volkswagen">Volks wagen</option>
                            <option value="Fiat">Fiat</option>
                                <option value="Toyouta">Toyota</option>
                                
                                    <option value="Bmw">BMW</option>
                                    <option value="Mazda">mazda</option>
                                      <option value="Kia">Kia</option>
                                        <option value="Audi">Audi</option>
                                    <option value="Peugeot">Peugeot</option>
                                      <option value="Nissan">Nissan</option>

                                       <option value="Renault">Renault</option>
                                    <option value="hyundai">Hyundai</option>
                                      <option value="Ford">Ford</option>
                                         <option value="Mercedes">Mercedes</option>
                            </select>
                          @if ($errors->has ('marque'))
                          <span class="help-block">{{ $errors->first('marque')}}</span>
                          @endif
                        </div>
            <br>

                           <div class="form-group {{ $errors->has('modele') ? 'has-error' :'' }}">

                        <label for="modele"><h4>Modele</h4> </label>
                        <select name="modele" value="{{old('modele') }}">
                            <option value="">Selectionner</option>
                            <option value="Panda">Panda</option>
                            <option value="Fiorino">Fiorino</option>
                                <option value="500 X">500 X</option>
                                
                                    <option value="Punto">Punto</option>
                                    <option value="Tipo">Tipo</option>
                            <option value="Talento">Talento</option>
                             <option value="Fullback">Fullback</option>
                            <option value="Serie 4">Serie 4</option>
                                <option value="Serie 3">Serie 3</option>
                                    <option value="Serie 1">Serie 1</option>
                                      <option value="X7">X7</option>
                                        <option value="Berlingo">Berlingo</option>
                                <option value="C-elysee">C-elysee</option>
                                    <option value="Spacetourer">Spacetourer</option>
                                        <option value="A1">A1</option>
                                              <option value="A1">A3</option>
                                <option value="A4">A4</option>
                                    <option value="A5">A5</option>
                                        <option value="A6">A6</option>
                                <option value="A7">A7</option>
                                   <option value="Q8">Q8</option>
                                     <option value="Q7">Q7</option>
                                    <option value="F150">F150</option>
                                         <option value="Puma">Puma</option>
                                         <option value="Ranger">Ranger</option>
                                          <option value="Grand Tourneo Connect">Grand Tourneo Connect</option>
                                            <option value="Expedition">Expedition</option>
                                <option value="Mustang">Mustang</option>
                                    <option value="Accord">Accord</option>
                                        <option value="Hr-v">Hr-v</option>
                                <option value="Niro">Niro</option>
                                    <option value="Optima">Optima</option>
                                        <option value="Ioniq">Ioniq</option>
                                    <option value="Kona">Kona</option>
                                        <option value="Santa Fe">Santa fe</option>
                                <option value="Citan">Citan</option>
                                    <option value="Gle">Gle</option>
                                            <option value="Vito">Vito</option>
                                    <option value="Cla">Cla</option>
                                        <option value="Classe E">Classe E</option>
                                <option value="301">301</option>
                                    <option value="208">208</option>
                                      <option value="2008">2008</option>
                                    <option value="Expert">Expert</option>
                                    <option value="Traveller">Traveller</option>
                                            <option value="5008">5008</option>
                                             <option value="Expert">Expert</option>
                                    <option value="Clio">Clio</option>
                                        <option value="Talisman">Talisman</option>
                                <option value="Kangoo">Kangoo</option>
                                    <option value="Alaskan">Alaskan</option>

                                            <option value="Hilux">Hilux</option>
                                        <option value="Gt86">Gt86</option>
                                          <option value="Avensis">Avensis</option>
                                            <option value="Land Cruiser">Land Cruiser</option>
                            <option value="3">3</option>
                                    <option value="6">6</option>
                                            <option value="Cx-5">Cx-5</option>
                                        <option value="Amarok">Amarok</option>
                                <option value=" Caddy">Caddy</option>
                                    <option value="Caravelle">Caravelle</option>
                                           <option value="Symbole">Symbole</option>
                                       <option value="Passat">Passat</option>
                                <option value="Polo">Polo</option>
                                    <option value="Golf">Golf</option>
                                           <option value="Nv200">Nv200</option>

                                            <option value="Navara">Navara</option>
                                      <option value="Qashqai">Qashqai</option>
                                <option value="370Z">370z</option>
                                    <option value="Juke">Juke</option>


                                            <option value="Nv200">Nv200</option>

                                            <option value="Navara">Navara</option>
                                      <option value="Qashqai">Qashqai</option>
                                <option value="370Z">370z</option>
                                    <option value="Trafic">Trafic</option>
                                         <option value="Clio">Clio</option>
                                           <option value="Alaskan">Alaskan</option>
                                            <option value="Kangoo Express">Kangoo Express</option>
                                         <option value="Megane">Megane</option>
                                          <option value="Talisman">Talisman</option>
                                           <option value="Edge">Edge</option>
                            </select>
                                  @if ($errors->has ('modele'))
                          <span class="help-block">{{ $errors->first('modele')}}</span>
                          @endif
                    
                </div>
            <br>
             <div class="form-group">

                        <label for="type"><h4>Type de voiture</h4> </label>
                        <select name="type" >
                            <option value="">Selectionner</option>
                            <option value="a">MOYENNE BERLINE</option>
                            <option value="b">PICk-UP</option>
                                <option value="c">SUV</option>
                                
                                    <option value="d">UTILITAIRE</option>
                            </select>
                                  @if ($errors->has ('type'))
                          <span class="help-block">{{ $errors->first('type')}}</span>
                          @endif
                          
                        </div>
            <br>
                <div class="form-group {{ $errors->has('url') ? 'has-error' :'' }}">
                 {{ Form::label('url', 'Image') }}
            {{ Form::file('url', null, array('class' => 'form-control')) }}
            <br>
                   @if ($errors->has ('url'))
                          <span class="help-block">{{ $errors->first('url')}}</span>
                          @endif
        </div>

         
        
                 <div class="form-group" style="width: 300px;">
            {{ Form::submit('Ajouter', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
           </div> 
        </div>
        </div>
    </div>

@endsection