

@extends('layouts.app')

@section('title', '| Edit User')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-user-plus'></i> Modifier {{$user->name}}</h1>
    <hr>

    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

    <div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
        {{ Form::label('name', 'Nom') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}

                @if ($errors->has ('name'))
                          <span class="help-block">{{ $errors->first('name')}}</span>
                          @endif
    </div>

   <div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, array('class' => 'form-control')) }}

                @if ($errors->has ('email'))
                          <span class="help-block">{{ $errors->first('email')}}</span>
                          @endif
    </div>

    <h5><b>Choisir role</b></h5>

    <div class='form-group'>
        @foreach ($roles as $role)
            {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

        @endforeach
    </div>

  <div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
        {{ Form::label('password', 'Mot de passe') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}

           @if ($errors->has ('password'))
                          <span class="help-block">{{ $errors->first('password')}}</span>
                          @endif

    </div>

    <div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
        {{ Form::label('password', 'Confirmer mot de passe') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

           @if ($errors->has ('password_confirmation'))
                          <span class="help-block">{{ $errors->first('password_confirmation')}}</span>
                          @endif

    </div>

    {{ Form::submit('Ajouter', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection