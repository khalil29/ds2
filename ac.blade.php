@extends('layouts.home')
@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
  </head>
<body>
        <section id="home" class="home">
      
            <div id="carousel" class="carousel slide" data-ride="carousel">
          
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="do/images/slider2_img.jpg" alt="Construction">
                        <div class="overlay">
                            <div class="carousel-caption">
                                <h3>Vous cherchez une voiture</h3>
                                <h1>Notre site Web est votre moyen de trouver une voiture</h1>
                                <h1 class="second_heading">facile à utiliser &  professionnelle</h1>
                             
                            </div>					
                        </div>
                    </div>
                    <div class="item">
                        <img src="do/images/slider3_img.jpg" alt="Construction">
                        <div class="overlay">
                            <div class="carousel-caption">
                                <h3>Vous cherchez une voiture</h3>
                                <h1>Notre site Web est votre moyen de trouver une voiture</h1>
                                <h1 class="second_heading">facile à utiliser &  professionnelle</h1>
                             
                            </div>					
                        </div>
                    </div>
                    <div class="item">
                        <img src="do/images/slider4_img.jpg" alt="Construction">
                        <div class="overlay">
                            <div class="carousel-caption">
                                <h3>Vous cherchez une voiture</h3>
                                <h1>Notre site Web est votre moyen de trouver une voiture</h1>
                                <h1 class="second_heading">facile à utiliser &  professionnelle</h1>
                             
                            </div>					
                        </div>
                    </div>
                </div>



             
                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </section>

 <section id="about">
            <div class="container ">
                <div class="row">
                    <div class="col-lg-7 col-md-6">
                        <div class="about_content">
                            <h2>Bienvenue dans notre agence</h2>
                         
                            <p>Notre agence a été établie il y a longtemps et connaît les bons services fournis aux clients.</p>
                            <p>Ce site vous permet de connaître l'adresse de notre agence et de réserver facilement une voiture.</p>

    
                              <p><h3>Nous espérons que vous aimez notre site!</h3></p>                           
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-lg-offset-1">
                        <div>
                            <img src="do/images/bn.jpg" style="width:400px;height: 400px;">
                        </div>
                    </div>
                </div>
            </div>
        </section>

                 <section id="services">
            <div class="container">
                <h2>Notre types</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="service_item">
                            <img src="img/coupe.png" style="width: 250px;height: 200px;">
                            <h3>MOYENNE BERLINE</h3>
                        
                            <a href="{!!url('showvo')!!}" class="btn know_btn">En savoir plus</a>
                        </div>
                    </div>
                        <div class="col-md-3">
                        <div class="service_item">
                            <img src="img/pu.png" style="width: 250px;height: 200px;">
                            <h3>PICK-UP</h3>
                          
                            <a href="{!!url('showvoo')!!}" class="btn know_btn">En savoir plus</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="service_item">
                            <img src="img/suvv.png" style="width: 250px;height: 200px;">
                            <h3>SUV</h3>
                          
                            <a href="{!!url('showvc')!!}" class="btn know_btn">En savoir plus</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="service_item">
                            <img src="img/ut.png" style="width: 250px;height: 200px;">
                            <h3>UTILITAIRE</h3>
                          
                            <a href="{!!url('showvd')!!}" class="btn know_btn">En savoir plus</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>




        <footer>
        
            <div class="container footer_top">
                <div class="row">
                    <div class="col-lg-4 col-sm-7">
                        <div class="footer_item">
                            <h4>Réseaux sociaux</h4>
                           
                     

                            <ul class="list-inline footer_social_icon">
                                <li><a href="https://www.facebook.com/Ulocation-2324948847542037/?modal=admin_todo_tour"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="https://twitter.com/Ulocation1"><span class="fa fa-twitter"></span></a></li>
                                <li><a href=""><span class="fa fa-youtube"></span></a></li>
                                <li><a href=""><span class="fa fa-google-plus"></span></a></li>
                                <li><a href=""><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-5">
                        <div class="footer_item">
                            <h4>Nos pages</h4>
                            <ul class="list-unstyled footer_menu">
                                <li><a href="ac"><span class="fa fa-play"></span> Home</a>
                                <li><a href="type1"><span class="fa fa-play"></span> Nos types</a>
                                <li><a href="agence"><span class="fa fa-play"></span> Notre agence</a>
                                <li><a href="{{route('newcomment')}}"><span class="fa fa-play"></span> Contacter nous</a>
                                <li><a href="reserverr"><span class="fa fa-play"></span> Réserver</a>
                             
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-7">
                        <div class="footer_item">
                            <h4>Nos types</h4>
                            <ul class="list-unstyled post">
                              <li><a href="{!!url('showvo')!!}"><span class="fa fa-play"></span> MOYENNE BERLINE</a>
                                <li><a href="{!!url('showvoo')!!}"><span class="fa fa-play"></span> PICK-UP</a>
                                <li><a href="{!!url('showvc')!!}"><span class="fa fa-play"></span> SUV</a>
                                <li><a href="{!!url('showvd')!!}"><span class="fa fa-play"></span> UTILITAIRE</a>
                              
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-5">
                        <div class="footer_item">
                            <h4>Contact nous</h4>
                            <ul class="list-unstyled footer_contact">
                                <li><a href=""><span class="fa fa-map-marker"></span> 55,Avenue hedi nouira, Ariana</a></li>
                                <li><a href=""><span class="fa fa-envelope"></span> agence@gmail.com</a></li>
                                <li><a href=""><span class="fa fa-mobile"></span>29307142</a></li>
                                  <li><a href=""><span class="fa fa-mobile"></span>29306854</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

       
        </footer> 
</section>

</body>
@endsection