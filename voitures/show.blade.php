@extends('layouts.app')

@section('title', '| View voiture')

@section('content')

<div class="container">

    <h1>{{ $voiture->marque }}</h1>
    <hr>
    {{ $voiture->modele }} </p>
    <hr>

    <h1>{{ $voiture->type }}</h1>
   
    {!! Form::open(['method' => 'DELETE', 'route' => ['voitures.destroy', $voiture->id] ]) !!}
    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
    @can('Edit Voiture')
    <a href="{{ route('voitures.edit', $voiture->id) }}" class="btn btn-info" role="button">Edit</a>
    @endcan
    @can('Delete Voiture')
    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
    @endcan
    {!! Form::close() !!}

</div>

@endsection