@extends('layouts.app')

@section('title', '| Add Role')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Ajouter un role</h1>
    <hr>

    {{ Form::open(array('url' => 'roles')) }}

     <div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
        {{ Form::label('name', 'Nom') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    @if ($errors->has ('name'))
                          <span class="help-block">{{ $errors->first('name')}}</span>
                          @endif
    </div>

    <h5><b>Choisir une permission</b></h5>

       <div class="form-group {{ $errors->has('permissions') ? 'has-error' :'' }}">
        @foreach ($permissions as $permission)
            {{ Form::checkbox('permissions[]',  $permission->id ) }}
            {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
               @if ($errors->has ('permissions'))
                          <span class="help-block">{{ $errors->first('permissions')}}</span>
                          @endif

        @endforeach

    </div>

    {{ Form::submit('Ajouter', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection