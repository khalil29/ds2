@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Les commentaires des clients</h3></div>
                   
                    
                    <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom de client</th>
                    <th>Prénom de client</th>
                    <th>Numéro de téléphone</th>
                    <th>Email</th>
                     <th>Commentaire</th>
                     <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($commentaires as $commentaire)
                <tr>
                       <td>{{ $commentaire->id }}</td>
                    <td>{{ $commentaire->nom }}</td>
                    <td>{{$commentaire->prenom }}</td>
                    <td>{{$commentaire->num_tele}}</td>
                     <td>{{$commentaire->email}}</td>
                      <td><p>{{$commentaire->title}}</p></td>
                      <td>           
 <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                <a href="{!!route('delete4',$commentaire->id)!!}" class="btn btn-danger btn-sm">Supprimer</a>
           

                    </td>
                   
                 
                </tr>
                @endforeach
            </tbody>

        </table>
                   
                </div>
            </div>
        </div>
@endsection