@extends('layouts.app')

@section('title', '| Edit Voiture')

@section('content')
<div class="row">

    <div class="col-md-8 col-md-offset-2">

        <h1><i class='fa fa-car'>Modifier :{{$voiture->modele}}</i></h1>
        <hr>
            {{ Form::model($voiture, array('route' => array('voitures.update', $voiture->id), 'method' => 'PUT')) }}
           <div class="form-group  {{ $errors->has('marque') ? 'has-error' :'' }}">
            {{ Form::label('marque', 'Marque') }}
            {{ Form::text('marque', null, array('class' => 'form-control')) }}<br>
                 @if ($errors->has ('marque'))
                          <span class="help-block">{{ $errors->first('marque')}}</span>
                          @endif
                      </div>
                     <div class="form-group  {{ $errors->has('modele') ? 'has-error' :'' }}">
             {{ Form::label('modele', 'Modele') }}
            {{ Form::text('modele', null, array('class' => 'form-control')) }}<br>

               @if ($errors->has ('modele'))
                          <span class="help-block">{{ $errors->first('modele')}}</span>
                          @endif
                      </div>
                        <div class="form-group  {{ $errors->has('type') ? 'has-error' :'' }}">
              {{ Form::label('type', 'Type') }}
            {{ Form::text('type', null, array('class' => 'form-control')) }}<br>
               @if ($errors->has ('type'))
                          <span class="help-block">{{ $errors->first('type')}}</span>
                          @endif
                      </div>
     <div class="form-group  {{ $errors->has('url') ? 'has-error' :'' }}">
              {{ Form::label('image', 'Image') }}
            {{ Form::file('url', null, array('class' => 'form-control')) }}<br>
             
                  @if ($errors->has ('url'))
                          <span class="help-block">{{ $errors->first('url')}}</span>
                          @endif
                      </div>
            {{ Form::submit('Enregistrer', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
    </div>
    </div>
</div>

@endsection