@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Liste des voitures</h3></div>
                    <div class="panel-heading">Page {{ $voitures->currentPage() }} of {{ $voitures->lastPage() }}</div>
                    
                    <table class="table table-bordered table-striped">

            <thead>
                <tr>
                
                    <th>Marque</th>
                    <th>Modéle</th>
                    <td>Nombre disponible</td>
                    <th>Etat</th>

                    <td>Action</td>
                </tr>
            </thead>

            <tbody>
                @foreach ($voitures as $voiture)
                <tr>

                    <td>{{ $voiture->marque }}</td>
                    <td>{{ $voiture->modele }}</td>
                      <td>{{ $voiture->nbre}}</td>
                    <td>{{ $voiture->etat_car}}</td>
               <td>
                    <a href="{{ route('editt', $voiture->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Modifier</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['voitures.destroy', $voiture->id] ]) !!}
                    {!! Form::submit('Supprimer', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
                    <div class="text-center">
                        {!! $voitures->links() !!}
                    </div>
                </div>
            </div>
        </div>
@endsection