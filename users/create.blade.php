
@extends('layouts.app')

@section('title', '| Add User')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

   <h3> <i class='fa fa-user-plus'></i> Ajouter un utilisateur</h3>
    <hr>

    {{ Form::open(array('url' => 'users')) }}

       <div class="form-group  {{ $errors->has('name') ? 'has-error' :'' }}">
        {{ Form::label('name', 'Nom') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
          @if ($errors->has ('name'))
                          <span class="help-block">{{ $errors->first('name')}}</span>
                          @endif
    </div>

 <div class="form-group  {{ $errors->has('email') ? 'has-error' :'' }}">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', '', array('class' => 'form-control')) }}
            @if ($errors->has ('email'))
                          <span class="help-block">{{ $errors->first('email')}}</span>
                          @endif
    </div>

        @foreach ($roles as $role)
         <div class="form-group  {{ $errors->has('roles') ? 'has-error' :'' }}">
            {{ Form::checkbox('roles[]',  $role->id ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                  @if ($errors->has ('roles'))
                          <span class="help-block">{{ $errors->first('roles')}}</span>
                          @endif
        @endforeach

    </div>

   <div class="form-group  {{ $errors->has('password') ? 'has-error' :'' }}">
        {{ Form::label('password', 'Mot de passe') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}
              @if ($errors->has ('password'))
                          <span class="help-block">{{ $errors->first('password')}}</span>
                          @endif

    </div>

 <div class="form-group  {{ $errors->has('password') ? 'has-error' :'' }}">
        {{ Form::label('password', 'Confirmer le mot de passe') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
      @if ($errors->has ('password'))
                          <span class="help-block">{{ $errors->first('password')}}</span>
                          @endif
    </div>

    {{ Form::submit('Ajouter', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection