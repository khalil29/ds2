@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Liste des voitures</h3></div><a href="{{ URL::to('voitures/create') }}" class="btn btn-success pull-right">Ajouter une voiture</a>
                    <div class="panel-heading">Page {{ $voitures->currentPage() }} of {{ $voitures->lastPage() }}</div>
                    
                    <table class="table table-bordered table-striped">

            <thead>
                <tr>
                
                    <th>Marque</th>
                    <th>Modéle</th>
                    <th>Type</th>
                    <th>Image</th>
                    <td>Action</td>
                </tr>
            </thead>

            <tbody>
                @foreach ($voitures as $voiture)
                <tr>

                    <td>{{ $voiture->marque }}</td>
                    <td>{{ $voiture->modele }}</td>
                    <td>{{ $voiture->type}}</td>
                    <td><img src="img/{{$voiture->url}}" style="width:120px; height: 80px; "> </td>
                    <td>
                    <a href="{{ route('voitures.edit', $voiture->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Modifier</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['voitures.destroy', $voiture->id] ]) !!}
                    {!! Form::submit('Supprimer', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
                    <div class="text-center">
                        {!! $voitures->links() !!}
                    </div>
                </div>
            </div>
        </div>
@endsection